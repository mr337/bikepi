BikePi
=====

BikePi is a software project that helps make biking safer and hopefully on a shoe string budget.

###Contact
Website: bikepi.org  
Bug Tracker: redmine.bikepi.org  
Mailing List: bikepi-mailinglist@bikepi.org  
Mailing List Web: https://mailman-mail5.webfaction.com/listinfo/bikepi-mailinglist  

###What Is BikePi
BikePi is open source software in making cycling safer. It is built around a rhaspberryPi, means efficient to run on a tiny board (still fast IMHO).

###Code
Code is licenceded under GPLv3.  
Git Repo: https://bitbucket.org/mr337/bikepi  
GitHub Mirror: https://github.com/mr337/BikePi 

###Installation
TBA

###Get Help
The best place to get help or have questions answered is the mailing list. Feel free to send out a question.

For more technical problem or reporting bugs please visit our bug tracker. Details on contact info is above under Contact.

###Contribute to BikePi
This is almost TBA. Something thing of course is code features or updates, for this use mailing list.


###Contributors
Lee Hicks (mr337)

###Other

####Alternatives
BikePi is an open source project aimed at making biking safer. In it's current state it is just software and some off the shelf hardware put together. If you are looking at something much more streamlined, tested, and "Just Works" then you may want to look at Cerevellum Hindsight or Owl 360. I can't recommend or not-recommend any of these products as I have not purchased them.

I do not know of any open source alternatives. If you know of one and would like me to add to the lease please email me, mr337 at mr337.com
