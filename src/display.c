/*
* 
* bikepi is free software: you can redistribute it and/or modify
* it under the terms of the gnu general public license as published by
* the free software foundation, either version 3 of the license, or
* (at your option) any later version.
* 
* bikepi is distributed in the hope that it will be useful,
* but without any warranty; without even the implied warranty of
* merchantability or fitness for a particular purpose.  see the
* gnu general public license for more details.
* 
* you should have received a copy of the gnu general public license
* along with bikepi.  if not, see <http://www.gnu.org/licenses/>.
*/

#include "display.h"

IplImage * generateMissingConfigImg(char * configPath)
{
    //generate backgroud
    MagickWand *mw = NewMagickWand();
    PixelWand *pw = NewPixelWand();
    DrawingWand *dw = NewDrawingWand();
    PixelSetColor(pw, "red");
    MagickNewImage(mw, 320, 240, pw);

    //draw missing config message
    PixelSetColor(pw, "white");
    DrawSetFillColor(dw, pw);
    DrawSetFont(dw, "Verdana");
    DrawSetFontSize(dw, 25);
    DrawSetStrokeColor(dw, pw);
    DrawAnnotation(dw, 30, 120, "Cannot find config file"); 
    DrawSetFontSize(dw, 16);

    //draw path of config
    DrawAnnotation(dw, 30, 160, configPath); 
    MagickDrawImage(mw, dw);
    MagickWriteImage(mw, "/tmp/error.png");

    IplImage *ipl;
    ipl = magic2Ipl(mw);

    //cleanup stuff
    DestroyMagickWand(mw);
    DestroyPixelWand(pw);
    DestroyDrawingWand(dw);

    return ipl;

    //TODO return image in OpenCV format (ipl?)
}

IplImage * magic2Ipl(MagickWand *mw)
{
    int width = MagickGetImageWidth(mw);
    int height = MagickGetImageHeight(mw);

    char * blob = malloc(width*height);
    MagickExportImagePixels(mw,0,0,width,height,"RGB", CharPixel, blob);
    IplImage *img = cvCreateImage(cvSize(width, height), IPL_DEPTH_32F, 3);
    return img;


}



//void Magick2Ipl(Image magicImage, IplImage* cvImage)
//{
//    int width= magicImage.size().width();
//    int height = magicImage.size().height();
//
//    byte* blob= new byte[cvImage->imageSize];
//    magicImage.write(0,0, width, height, "BGRA", MagickCore::CharPixel, blob);
//    memcpy(cvImage->imageData, blob, cvImage->imageSize);
//    delete [] blob;
//}
