/*
* 
* bikepi is free software: you can redistribute it and/or modify
* it under the terms of the gnu general public license as published by
* the free software foundation, either version 3 of the license, or
* (at your option) any later version.
* 
* bikepi is distributed in the hope that it will be useful,
* but without any warranty; without even the implied warranty of
* merchantability or fitness for a particular purpose.  see the
* gnu general public license for more details.
* 
* you should have received a copy of the gnu general public license
* along with bikepi.  if not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"

int main( int argc, char **argv )
{
    //init opencv stuff
    CvCapture *webcam;
    IplImage *frame;

    //get configuartion settings from ~/.bikepi
    char path [100] = "\0";
    strncat(path, getHomeDir(),99);
    strncat(path,"/.bikepi/settings.cfg", 99);

    //init config struct
    config c = {0,0,0,0};
    c.images_path = (char *)calloc(1,150);

    //load config
    if(getConfig(path,&c))
    {
        printf("Cannot read config, ~/.bikepi/settings.cfg exist?\n");
        IplImage *error = generateMissingConfigImg(path);
        cvShowImage( "gui", error);
        cvWaitKey(1);
        exit(EXIT_FAILURE);
    }

    //init webcam, set resolution etc
    if(setupWebcam(&webcam, &c)){
        fprintf(stderr, "Cannot open webcam.\n");
        exit(EXIT_FAILURE);
    }
    cvNamedWindow("gui", CV_WINDOW_AUTOSIZE);

    char frame_path [150] = "\0";
    int frame_num = 1;
	int img_num = 1;
    int path_size = strlen(c.images_path);
    printf("%s", c.images_path);

    while(1)
    {
        //get the img from the webcam
        frame = cvQueryFrame(webcam);
        frame_num++;
        if( !frame ) break;
        cvShowImage( "gui", frame );
        cvWaitKey(1);

        if (frame_num % c.save_x_frame == 0){
            snprintf(frame_path, 149, c.images_path, img_num);
            cvSaveImage(frame_path, frame, NULL);
            img_num++;
        }

    }

    cvDestroyWindow("gui");
    cvReleaseCapture(&webcam);

    exit(EXIT_SUCCESS);
}

int setupWebcam(CvCapture **device, config *settings)
{
    *device = cvCaptureFromCAM(0);
    if(!*device) 
        return -1;

    //need to add to config file
    cvSetCaptureProperty(*device, CV_CAP_PROP_FRAME_WIDTH, settings->frame_width);
    cvSetCaptureProperty(*device, CV_CAP_PROP_FRAME_HEIGHT, settings->frame_height);

    return 0;
}

char * getHomeDir()
{
    //first let us try HOME env var
    char * path;
    path = getenv("HOME");
    if(path != NULL) {
        return path;
    }
}

int getConfig(char *path, config *settings)
{
    config_t cfg;
    config_setting_t *root, *value;
    config_init(&cfg);

    if(!config_read_file(&cfg, path)){
        config_destroy(&cfg);
        return -1; //could not read file
    }

    const char * img_path;
    const char * img_name;

   
    //fill values for struct config
    root = config_root_setting(&cfg);

    //for fps
    config_setting_lookup_int(root, "FPS", &settings->fps);

    //for saving ever Xth frmae
    config_setting_lookup_int(root, "SAVE_EVERY_X_FRAME", &settings->save_x_frame);

    //for frame_width
    config_setting_lookup_int(root, "FRAME_WIDTH", &settings->frame_width);

    //for frame_height
    config_setting_lookup_int(root, "FRAME_HEIGHT", &settings->frame_height);

    //for images_path 
    config_setting_lookup_string(root, "IMAGES_PATH", &img_path);

    //for images_filename
    config_setting_lookup_string(root, "IMAGES_NAME", &img_name); 

    //build image_path and name
    strncat(settings->images_path, img_path, 100);
    strncat(settings->images_path, img_name, 49);

    config_destroy(&cfg);

    if(!value){
        return -2; //settings does not exist
    }
    else{
        return 0;
    }

}
