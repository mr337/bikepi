/*
* 
* bikepi is free software: you can redistribute it and/or modify
* it under the terms of the gnu general public license as published by
* the free software foundation, either version 3 of the license, or
* (at your option) any later version.
* 
* bikepi is distributed in the hope that it will be useful,
* but without any warranty; without even the implied warranty of
* merchantability or fitness for a particular purpose.  see the
* gnu general public license for more details.
* 
* you should have received a copy of the gnu general public license
* along with bikepi.  if not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <libconfig.h>
#include <pwd.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include "cv.h"
#include "highgui.h"
#include "display.h"

typedef struct
{
    int fps;
    int save_x_frame;
    int frame_width;
    int frame_height;
    char * images_path;

} config;

int setupWebcam(CvCapture **device, config *settings);
char * getHomeDir();
int getConfig(char *path, config *settings);
